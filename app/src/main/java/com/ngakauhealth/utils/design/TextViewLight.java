package com.ngakauhealth.utils.design;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


public class TextViewLight extends AppCompatTextView {


    private Typeface typeface;

    public TextViewLight(Context context) {
        super(context);
        setCustomFont(context);
    }

    public TextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    public void setCustomFont(Context context) {

        try {
            if (typeface == null) {
                typeface = Typeface.createFromAsset(context.getAssets(), "textlight.otf");
                setTypeface(typeface);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}