package com.ngakauhealth.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.TextViewBold;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExamResultAdapter extends RecyclerView.Adapter<ExamResultAdapter.ViewHolder> {
    ArrayList<String> arrayListExamTitle;


    public ExamResultAdapter(ArrayList<String> arrayListExamTitle) {
        this.arrayListExamTitle = arrayListExamTitle;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_etr_exam_result, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textTitle.setText(arrayListExamTitle.get(position));

    }

    @Override
    public int getItemCount() {
        return arrayListExamTitle.size();
    }

    @OnClick({R.id.btn_add, R.id.btn_view, R.id.btn_graph})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                break;
            case R.id.btn_view:
                break;
            case R.id.btn_graph:
                break;
        }
    }

    static

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_add)
        LinearLayout btnAdd;
        @BindView(R.id.btn_view)
        LinearLayout btnView;
        @BindView(R.id.btn_graph)
        LinearLayout btnGraph;
        @BindView(R.id.text_title)
        TextViewBold textTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
