package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Profile2Fragment extends BaseFragment {


    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.edit_address)
    EditTextLight editAddress;
    @BindView(R.id.edit_phn_no)
    EditTextLight editPhnNo;
    @BindView(R.id.edit_ethnicity)
    EditTextLight editEthnicity;
    @BindView(R.id.spinner_ethnicity)
    Spinner spinnerEthnicity;
    @BindView(R.id.edit_ethnicity1)
    EditTextLight editEthnicity1;
    @BindView(R.id.edit_ethnicity2)
    EditTextLight editEthnicity2;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile2, container, false);


        unbinder = ButterKnife.bind(this, view);
        return view;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.back_btn, R.id.btn_next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                getLoginActivityContainter().displayIt(new Profile1Fragment(), R.id.loginContainer, "", false);

                break;
            case R.id.btn_next:
                break;
        }
    }
}
