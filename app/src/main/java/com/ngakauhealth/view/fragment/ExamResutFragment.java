package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.adapter.ExamResultAdapter;
import com.ngakauhealth.view.base.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ExamResutFragment extends BaseFragment {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_enter_all_result)
    ButtonBold btnEnterAllResult;
    @BindView(R.id.text_skip)
    TextViewLight textSkip;
    Unbinder unbinder;

    ArrayList<String> arrayListExamTitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_exam_result, container, false);


        unbinder = ButterKnife.bind(this, view);
        arrayListExamTitle = new ArrayList<>();
        arrayListExamTitle.add(getString(R.string.bmi));
        arrayListExamTitle.add(getString(R.string.bloodpressure));
        arrayListExamTitle.add(getString(R.string.cholesterol_level));
        arrayListExamTitle.add(getString(R.string.diabetes));
        arrayListExamTitle.add(getString(R.string.kidney_fun));


        initRecylerview();

        String uData = getString(R.string.skip_sec);
        SpannableString content = new SpannableString(uData);
        content.setSpan(new UnderlineSpan(), 0, uData.length(), 0);
        textSkip.setText(content);

        return view;

    }


    void initRecylerview() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        recyclerView.setNestedScrollingEnabled(false);

        ExamResultAdapter examResultAdapter = new ExamResultAdapter(arrayListExamTitle);
        recyclerView.setAdapter(examResultAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_enter_all_result, R.id.text_skip})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_enter_all_result:
                getHomeActivityContainter().displayIt(new BMIFragment(), R.id.home_container, "", true);

                break;
            case R.id.text_skip:
                break;
        }
    }
}
