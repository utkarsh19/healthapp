package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.model.HealthBgModel;
import com.ngakauhealth.view.adapter.HealthBgAdapter;
import com.ngakauhealth.view.base.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.ngakauhealth.utils.IConstants.POSITION;

public class HealthBgPagerFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private Unbinder unbinder;
    ArrayList<String> stringArrayList;
    int position = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_healthbg_pager, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null) {
            position = getArguments().getInt(POSITION);
            Log.d("position", "onCreateView: " + position);
        }

        quesdata();
        initRecylerview();

        return view;
    }

    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    void initRecylerview() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        recyclerView.setNestedScrollingEnabled(false);

        HealthBgAdapter healthBgAdapter = new HealthBgAdapter(getActivity(), setData(position).get(position), 0);
        recyclerView.setAdapter(healthBgAdapter);
    }


    HashMap<Integer, ArrayList<HealthBgModel>> setData(int pos) {
        HashMap<Integer, ArrayList<HealthBgModel>> map = new HashMap<>();
        ArrayList<String> options = new ArrayList<>();
        ArrayList<String> ques = new ArrayList<>();
        ArrayList<HealthBgModel> healthBgModelArrayList = new ArrayList<>();

        HealthBgModel healthBgModel = new HealthBgModel();
        ques.add("nkjnjff");
        healthBgModel.setQues(ques);
        options.add("Yes");
        options.add("No");
        healthBgModel.setOptions(options);
        healthBgModelArrayList.add(healthBgModel);

        healthBgModel = new HealthBgModel();
        options = new ArrayList<>();
        ques = new ArrayList<>();
        ques.add("nkjnjff33");
        healthBgModel.setQues(ques);
        options.add("Ye33s");
        options.add("No33");
        healthBgModel.setOptions(options);
        healthBgModelArrayList.add(healthBgModel);

        map.put(0, healthBgModelArrayList);

        options = new ArrayList<>();
        ques = new ArrayList<>();
        healthBgModelArrayList = new ArrayList<>();
        healthBgModel = new HealthBgModel();
        ques.add("nkjnjff");
        healthBgModel.setQues(ques);
        options.add("Yes1");
        options.add("No1");
        options.add("No1");
        healthBgModel.setOptions(options);
        healthBgModelArrayList.add(healthBgModel);
        map.put(1, healthBgModelArrayList);


        options = new ArrayList<>();
        ques = new ArrayList<>();
        healthBgModelArrayList = new ArrayList<>();
        healthBgModel = new HealthBgModel();
        ques.add("nkjnj33ff");
        healthBgModel.setQues(ques);
        options.add("Y33es1");
        options.add("No331");
        options.add("No331");
        healthBgModel.setOptions(options);
        healthBgModelArrayList.add(healthBgModel);
        map.put(2, healthBgModelArrayList);

        options = new ArrayList<>();
        ques = new ArrayList<>();
        healthBgModelArrayList = new ArrayList<>();
        healthBgModel = new HealthBgModel();
        ques.add("nkjnj44ff");
        healthBgModel.setQues(ques);
        options.add("Yes441");
        options.add("N44o1");
        options.add("No441");
        healthBgModel.setOptions(options);
        healthBgModelArrayList.add(healthBgModel);
        map.put(3, healthBgModelArrayList);

        return map;
    }


    void quesdata() {
        stringArrayList = new ArrayList<>();
        stringArrayList.add("Have your parents ,siblings or children been diagnosed with type II Diabetes?");
        stringArrayList.add("Have your parents ,siblings or children at an early age(younger than age 50) had a heart attack or stroke?");
        stringArrayList.add("Have you ever suffered from any heart disease such as Angina, stroke,chest pain or heart attack?");
        stringArrayList.add("Do you have Type II Diabetes?");
        stringArrayList.add("Do you suffer from any of the following conditions?");
        stringArrayList.add("Do you suffer from renal complications?");
        stringArrayList.add("Do you smoke cigarettes?");

        stringArrayList.add("Do you suffer from anxiety or depression?");
        stringArrayList.add("Diagnosed Depression?");
        stringArrayList.add("Diagnosed Schizophrenia?");
        stringArrayList.add("Which of the following statements best describes your usual weekly physical activity?");
        stringArrayList.add("How many servings of fruit and vegetables do you eat in a typical day?");
    }


}
