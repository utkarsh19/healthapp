package com.ngakauhealth.utils.design;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.Singleton;

import java.lang.reflect.Field;


public class EditTextLight extends AppCompatEditText implements EditTextValidator {

    int sCharchterLength = 150;
    protected boolean emptyAllowed;
    protected int testType;
    protected String testErrorString;
    private Typeface typeface;
    protected String emptyErrorString;
    protected EditText editText;
    private String blockCharacterSet = "~#^|$%&*!=*()-";
    TextWatcher mTextWatcher;
    Spinner mSpinner = new Spinner(getContext());
    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    public static InputFilter EMOJI_FILTER = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
            }
            return null;
        }
    };


    InputFilter toUpperCaseFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = start; i < end; i++) {
                Character character = source.charAt(start);
                if (i == 0) {
                    character = Character.toUpperCase(character); // THIS IS UPPER CASING
                }
                stringBuilder.append(character);
            }
            return stringBuilder.toString();
        }
    };


    public EditTextLight(Context context) {
        super(context);
    }

    public EditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public EditTextLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    public void setCustomFont(Context context, AttributeSet set) {

        try {

            TypedArray typedArray = context.obtainStyledAttributes(set, R.styleable.EditTextHealth);
            editText = this;
            if (typeface == null) {
                typeface = Typeface.createFromAsset(context.getAssets(), "AEH.ttf");
                emptyAllowed = typedArray.getBoolean(R.styleable.EditTextHealth_emptyAllowed, false);
                testType = typedArray.getInt(R.styleable.EditTextHealth_testType, EditTextValidator.TEST_NOCHECK);
                testErrorString = typedArray.getString(R.styleable.EditTextHealth_testErrorString);
                emptyErrorString = typedArray.getString(R.styleable.EditTextHealth_emptyErrorString);
                sCharchterLength = typedArray.getInteger(R.styleable.EditTextHealth_length, sCharchterLength);
                if (testType == TEST_PERSONNAME) {
                    this.setFilters(new InputFilter[]{filter, EMOJI_FILTER});
                    this.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    setCapitalizeTextWatcher(this);
                } else if (testType == TEST_ALPHA) {
                    this.setFilters(new InputFilter[]{filter, EMOJI_FILTER});
                } else if (testType == TEST_CHARCHETER_LENGTH) {
                    this.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(3)});
                } else {
                    this.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(sCharchterLength)});
                }

                setTypeface(typeface);
            }

            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(this, R.drawable.cursor_drawable);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void setCapitalizeTextWatcher(final EditText editText) {
        final TextWatcher textWatcher = new TextWatcher() {

            int mStart = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mStart = start + count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString();
                String capitalizedText;
                if (input.length() < 1)
                    capitalizedText = input;
                else
                    capitalizedText = input.substring(0, 1).toUpperCase() + input.substring(1);
                if (!capitalizedText.equals(editText.getText().toString())) {
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            editText.setSelection(mStart);
                            editText.removeTextChangedListener(this);
                        }
                    });
                    editText.setText(capitalizedText);
                }
            }
        };

        editText.addTextChangedListener(textWatcher);
    }


    public void setEditText(EditText editText) {

        this.addTextChangedListener(getTextWatcher());
    }

    @Override
    public TextWatcher getTextWatcher() {
        if (mTextWatcher == null) {
            mTextWatcher = new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    if (!TextUtils.isEmpty(s)) {
                        try {

                            Singleton.ToastMsg(getContext(), getContext().getString(R.string.error_field_must_not_be_empty));

                        } catch (Throwable e) {
                            editText.setError(null);
                        }
                    }

                }
            };
        }

        return null;
    }

    @Override
    public boolean isEmptyAllowed() {
        return false;
    }

    @Override
    public void resetValidators(Context context) {


    }

    @Override
    public boolean testValidity() {
        return false;
    }

    @Override
    public boolean testValidity(boolean showUIError) {
        return false;
    }

    @Override
    public void showUIError() {

    }


    public boolean validate(AppCompatActivity mAppCompatActivity) {
        String message = "";
        boolean isValid = true;

        if (this.getText().toString().trim().length() == 0) {
            message = this.editText.getHint() + " " + getContext().getString(R.string.error_field_must_not_be_empty);
            isValid = false;
        } else {

            switch (testType) {

                case TEST_ALPHANUMERIC:
                    isValid = RegexpValidator.validateAlphaNumeric(this.editText);
                    if (!isValid)
                        message = this.editText.getHint() + " " + getContext().getString(R.string.error_date_not_valid);
                    break;

                case TEST_NUMERIC:
                    isValid = TextUtils.isDigitsOnly(this.editText.getText().toString().trim());
                    if (!isValid)
                        message = this.editText.getHint() + " " + getContext().getString(R.string.error_date_not_valid);
                    break;
                case TEST_NUMERIC_RANGE:

                    break;
                case TEST_FLOAT_NUMERIC_RANGE:

                    break;
                case TEST_REGEXP:

                    break;
                case TEST_CREDITCARD:
                    isValid = RegexpValidator.validateCardNumber(this.editText);
                    if (!isValid)
                        message = this.editText.getHint() + "" + getContext().getString(R.string.error_creditcard_number_not_valid);
                    break;
                case TEST_EMAIL:
                    isValid = RegexpValidator.isEmailValid(this.editText);
                    if (!isValid)
                        message = this.editText.getHint() + " " + getContext().getString(R.string.error_email_address_not_valid);
                    break;
                case TEST_PHONE:
                    isValid = RegexpValidator.validatePhoneNo(this.editText);
                    if (!isValid)
                        message = this.editText.getHint() + " " + getContext().getString(R.string.error_phone_not_valid);
                    break;
                case TEST_DOMAINNAME:

                    break;
                case TEST_IPADDRESS:

                    break;
                case TEST_WEBURL:

                    break;
                case TEST_PERSONNAME:

                    isValid = RegexpValidator.validatePersonName(this.editText);
                    if (!isValid)
                        message = this.editText.getHint() + " " + getContext().getString(R.string.error_notvalid_personname);
                    break;
                case TEST_PERSONFULLNAME:

                    break;

                case TEST_CUSTOM:


                    break;

                case TEST_DATE:

                    break;

                case TEST_PASSWORD_LENGTH:
                    isValid = RegexpValidator.isPasswordLength(this.editText);
                    if (!isValid)
                        message = getContext().getString(R.string.passwordLength);
                    break;
            }

        }


        if (message.length() > 0) {
            Singleton.ToastMsg(getContext(), message);

        }

        return isValid;


    }
}