package com.ngakauhealth.view.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.view.activity.HomeContainerActivity;
import com.ngakauhealth.view.activity.LoginContainerActivity;

public class BaseFragment extends Fragment {


    LoginContainerActivity loginContainerActivity;
    HomeContainerActivity homeContainerActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoginContainerActivity)
            loginContainerActivity = (LoginContainerActivity) context;
        if (context instanceof HomeContainerActivity)
            homeContainerActivity = (HomeContainerActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public LoginContainerActivity getLoginActivityContainter() {
        if (loginContainerActivity != null)
            return loginContainerActivity;
        else
            return (LoginContainerActivity) getActivity();
    }


    public HomeContainerActivity getHomeActivityContainter() {
        if (homeContainerActivity != null)
            return homeContainerActivity;
        else
            return (HomeContainerActivity) getActivity();
    }


}
