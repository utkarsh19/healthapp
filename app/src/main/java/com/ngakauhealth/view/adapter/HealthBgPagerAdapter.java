package com.ngakauhealth.view.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ngakauhealth.view.fragment.HealthBgPagerFragment;

import static com.ngakauhealth.utils.IConstants.POSITION;

public class HealthBgPagerAdapter extends FragmentStatePagerAdapter {


    public HealthBgPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new HealthBgPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
