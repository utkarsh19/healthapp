package com.ngakauhealth.utils.design;

import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by netset on 14/12/17.
 */

public class RegexpValidator {

    public static boolean validateAlphaNumeric(EditText text){
        return Pattern.compile("[a-zA-Z0-9\u00C0-\u00FF \\./-\\?]*").matcher(text.getText().toString().trim()).matches();
    }


    /**
     * Validates the credit card number using the Luhn algorithm
     *
     * @param cardNumber the credit card number
     * @return
     */
    public static boolean validateCardNumber(EditText cardNumber) throws NumberFormatException {
        int sum = 0, digit, addend = 0;
        boolean doubled = false;
        for (int i = cardNumber.length() - 1; i >= 0; i--) {
            digit = Integer.parseInt(cardNumber.getText().toString().trim().substring(i, i + 1));
            if (doubled) {
                addend = digit * 2;
                if (addend > 9) {
                    addend -= 9;
                }
            } else {
                addend = digit;
            }
            sum += addend;
            doubled = !doubled;
        }
        return (sum % 10) == 0;
    }


    public static boolean isEmailValid(EditText text){
        return Patterns.EMAIL_ADDRESS.matcher(text.getText().toString().trim()).matches();
    }


    public static boolean validatePersonName(EditText text){
        return Pattern.compile("[\\p{L}-]+").matcher(text.getText().toString().trim()).matches();
    }


    public static boolean validatePhoneNo(EditText text){
        return Pattern.compile("(\\+[0-9]+[\\- \\.]*)?"                    // +<digits><sdd>*
                + "(\\([0-9]+\\)[\\- \\.]*)?"               // (<digits>)<sdd>*
                + "([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])").matcher(text.getText().toString().trim()).matches();
    }

    public  static boolean isValid(EditText text) {
        return TextUtils.getTrimmedLength(text.getText()) > 0;
    }

    public  static boolean isPasswordLength(EditText text) {
        return text.getText().toString().trim().length() > 5;
    }
}
