package com.ngakauhealth.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.CustomReusingDialog;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ForgotPassFragment extends BaseFragment {


    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.edit_email)
    EditTextLight editEmail;
    @BindView(R.id.btn_reset)
    ButtonBold btnReset;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_forgot_pass, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.back_btn, R.id.btn_reset})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:

                break;
            case R.id.btn_reset:
                CustomReusingDialog.Builder cBuilder = new CustomReusingDialog.Builder(getContext(), new CustomReusingDialog.OnDialogShowListener() {
                    @Override
                    public void onGetDialog(Dialog mDialog, View mView) {


                    }
                }).setCancelAble(false)
                        .setDialogLayout(R.layout.dialog_description);
                CustomReusingDialog mCustomReusingDialog = CustomReusingDialog.newInstance(cBuilder);
                mCustomReusingDialog.setCancelable(true);

                mCustomReusingDialog.show(getLoginActivityContainter().getSupportFragmentManager(), mCustomReusingDialog.getClass().getSimpleName());


                break;
        }
    }
}
