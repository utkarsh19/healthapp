package com.ngakauhealth.utils.design;

import android.content.Context;
import android.text.TextWatcher;

/**
 * Created by netset on 14/12/17.
 */

public interface EditTextValidator {


    TextWatcher getTextWatcher();

    boolean isEmptyAllowed();

    void resetValidators(Context context);

    boolean testValidity();

    boolean testValidity(boolean showUIError);

    void showUIError();

    int TEST_REGEXP = 0;

    int TEST_NUMERIC = 1;

    int TEST_ALPHA = 2;

    int TEST_ALPHANUMERIC = 3;

    int TEST_EMAIL = 4;

    int TEST_CREDITCARD = 5;

    int TEST_PHONE = 6;

    int TEST_DOMAINNAME = 7;

    int TEST_IPADDRESS = 8;

    int TEST_WEBURL = 9;

    int TEST_NOCHECK = 10;

    int TEST_CUSTOM = 11;

    int TEST_PERSONNAME = 12;

    int TEST_PERSONFULLNAME = 13;

    int TEST_DATE = 14;

    int TEST_NUMERIC_RANGE = 15;

    int TEST_FLOAT_NUMERIC_RANGE = 16;

    int TEST_PASSWORD_LENGTH=17;

    int TEST_CHARCHETER_LENGTH=18;

}
