package com.ngakauhealth.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ArcProgress;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.CustomReusingDialog;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class KidenyFragment extends BaseFragment {


    @BindView(R.id.edit_serum)
    EditTextLight editSerum;
    @BindView(R.id.edit_albuminuria)
    EditTextLight editAlbuminuria;
    @BindView(R.id.btn_submit)
    ButtonBold btnSubmit;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kidney, container, false);

        unbinder = ButterKnife.bind(this, view);


        return view;
    }


    @OnClick(R.id.btn_submit)
    public void onClick() {

        CustomReusingDialog.Builder cBuilder = new CustomReusingDialog.Builder(getContext(), new CustomReusingDialog.OnDialogShowListener() {
            @Override
            public void onGetDialog(final Dialog mDialog, View mView) {

                ButtonBold btnGoToHome = mView.findViewById(R.id.btn_gotohome);
                ArcProgress arcProgress = mView.findViewById(R.id.arc_progress);


                btnGoToHome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });


            }
        }).setCancelAble(false)
                .setDialogLayout(R.layout.dialog_risk_estimation);
        CustomReusingDialog mCustomReusingDialog = CustomReusingDialog.newInstance(cBuilder);
        mCustomReusingDialog.setCancelable(true);

        mCustomReusingDialog.show(getHomeActivityContainter().getSupportFragmentManager(), mCustomReusingDialog.getClass().getSimpleName());


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
