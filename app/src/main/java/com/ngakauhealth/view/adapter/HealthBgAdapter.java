package com.ngakauhealth.view.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.model.HealthBgModel;
import com.ngakauhealth.utils.design.TextViewLight;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HealthBgAdapter extends RecyclerView.Adapter<HealthBgAdapter.ViewHolder> {
    private int quesPos = 0;
    ArrayList<HealthBgModel> healthArrayListHashMap;
    FragmentActivity activity;


    public HealthBgAdapter(FragmentActivity activity, ArrayList<HealthBgModel> HealthArrayListHashMap, int quesPos) {
        this.healthArrayListHashMap = HealthArrayListHashMap;
        this.quesPos = quesPos;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_bg, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.textQues1.setText(healthArrayListHashMap.get(position).getQues().get(0));
        initRecylerview(holder.recyclerViewCheckBox, activity,healthArrayListHashMap.get(position).getOptions());
    }

    void initRecylerview(RecyclerView recyclerView, Activity activity, ArrayList<String> options) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new
                DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL));

        recyclerView.setNestedScrollingEnabled(false);

        HealthBgChkboxAdapter healthBgAdapter = new HealthBgChkboxAdapter(options);
        recyclerView.setAdapter(healthBgAdapter);
    }

    @Override
    public int getItemCount() {
        return healthArrayListHashMap.size();
    }

    static public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_ques1)
        TextViewLight textQues1;
        @BindView(R.id.recycler_view_check_box)
        RecyclerView recyclerViewCheckBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
