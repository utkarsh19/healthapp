package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CholesterolFragment extends BaseFragment {


    @BindView(R.id.edit_cholestero)
    EditTextLight editCholestero;
    @BindView(R.id.edit_hdlc)
    EditTextLight editHdlc;
    @BindView(R.id.text_hdl_ratio)
    TextViewLight textHdlRatio;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cholesterol, container, false);


        unbinder = ButterKnife.bind(this, view);

        String s = getString(R.string.hint_hdl_ratio);
        SpannableString ss1 = new SpannableString(s);
        ss1.setSpan(new RelativeSizeSpan(2f), 0, 3, 0); // set size
        ss1.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.purpleColor)), 0, 3, 0);// set color
        textHdlRatio.setText(ss1);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_next)
    public void onClick() {


        getHomeActivityContainter().displayIt(new DiabetesFragment(), R.id.home_container, "", true);

    }
}
