package com.ngakauhealth.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.CustomReusingDialog;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewBold;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RegisterFragment extends BaseFragment {


    @BindView(R.id.text_register)
    TextViewBold textRegister;
    @BindView(R.id.edit_fname)
    EditTextLight editFname;
    @BindView(R.id.edit_lname)
    EditTextLight editLname;
    @BindView(R.id.edit_email)
    EditTextLight editEmail;
    @BindView(R.id.edit_pass)
    EditTextLight editPass;
    @BindView(R.id.edit_repass)
    EditTextLight editRepass;
    @BindView(R.id.btn_reg)
    ButtonBold btnReg;
    Unbinder unbinder;
    @BindView(R.id.text_backtologin)
    TextViewLight textBacktologin;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);


        unbinder = ButterKnife.bind(this, view);

        String uData = getString(R.string.backtologin);
        SpannableString content = new SpannableString(uData);
        content.setSpan(new UnderlineSpan(), 0, uData.length(), 0);
        textBacktologin.setText(content);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.text_backtologin, R.id.btn_reg})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_backtologin:
                getLoginActivityContainter().displayIt(new LoginFragment(), R.id.loginContainer, "", false);

                break;
            case R.id.btn_reg:


                CustomReusingDialog.Builder cBuilder = new CustomReusingDialog.Builder(getContext(), new CustomReusingDialog.OnDialogShowListener() {
                    @Override
                    public void onGetDialog(final Dialog mDialog, View mView) {
                        EditTextLight edit1 = mView.findViewById(R.id.edit1);
                        EditTextLight edit2 = mView.findViewById(R.id.edit2);
                        EditTextLight edit3 = mView.findViewById(R.id.edit3);
                        EditTextLight edit4 = mView.findViewById(R.id.edit4);
                        ButtonBold btnSubmit = mView.findViewById(R.id.btn_submit);
                        TextViewLight resenCode = mView.findViewById(R.id.resen_code);

                        String uData = getString(R.string.resendCode);
                        SpannableString content = new SpannableString(uData);
                        content.setSpan(new UnderlineSpan(), 0, uData.length(), 0);
                        resenCode.setText(content);


                        btnSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                                getLoginActivityContainter().displayIt(new Profile1Fragment(), R.id.loginContainer, "", false);

                            }
                        });


                    }
                }).setCancelAble(false)
                        .setDialogLayout(R.layout.dialog_email_verify);
                CustomReusingDialog mCustomReusingDialog = CustomReusingDialog.newInstance(cBuilder);
                mCustomReusingDialog.setCancelable(true);

                mCustomReusingDialog.show(getLoginActivityContainter().getSupportFragmentManager(), mCustomReusingDialog.getClass().getSimpleName());


                break;
        }
    }


}
