package com.ngakauhealth.utils.design;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TextViewBold extends AppCompatTextView {


    private Typeface typeface;

    public TextViewBold(Context context) {
        super(context);
        setCustomFont(context);
    }

    public TextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public TextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }


    public void setCustomFont(Context context) {

        try {
            if (typeface == null) {
                typeface = Typeface.createFromAsset(context.getAssets(), "AEH.ttf");
                setTypeface(typeface);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (text != null) {
            if (text.length() > 0) {
                text = String.valueOf(text.charAt(0)).toUpperCase() + text.subSequence(1, text.length());
            }
            super.setText(text, type);
        }
    }

}