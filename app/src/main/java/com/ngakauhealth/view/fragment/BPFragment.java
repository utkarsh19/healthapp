package com.ngakauhealth.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BPFragment extends BaseFragment {


    @BindView(R.id.edit_bp1)
    EditTextLight editBp1;
    @BindView(R.id.edit_bp2)
    EditTextLight editBp2;
    @BindView(R.id.edit_pulse)
    EditTextLight editPulse;
    @BindView(R.id.text_avg_bp)
    TextViewLight textAvgBp;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_bp, container, false);


        unbinder = ButterKnife.bind(this, view);

        String s = getString(R.string.hint_avg_bp);
        SpannableString ss1 = new SpannableString(s);
        ss1.setSpan(new RelativeSizeSpan(2f), 0, 3, 0); // set size
        ss1.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.purpleColor)), 0, 3, 0);// set color
        textAvgBp.setText(ss1);


        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_next)
    public void onClick() {

        getHomeActivityContainter().displayIt(new CholesterolFragment(), R.id.home_container, "", true);

    }
}
