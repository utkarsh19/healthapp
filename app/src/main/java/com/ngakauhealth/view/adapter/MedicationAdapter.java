package com.ngakauhealth.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.TextViewBold;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicationAdapter extends RecyclerView.Adapter<MedicationAdapter.ViewHolder> {

    ArrayList<String> reminderList;

    public MedicationAdapter(ArrayList<String> reminderList) {

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_medication, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textMedicationName.setText(reminderList.get(position));
    }


    public void addMedication(ArrayList<String> reminderList) {
        this.reminderList.clear();
        this.reminderList.addAll(reminderList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return reminderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_medication_name)
        TextViewBold textMedicationName;
        @BindView(R.id.img_noti)
        ImageView imgNoti;
        @BindView(R.id.img_del)
        ImageView imgDel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
