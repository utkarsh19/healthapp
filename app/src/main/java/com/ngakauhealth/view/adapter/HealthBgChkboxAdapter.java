package com.ngakauhealth.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.TextViewLight;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HealthBgChkboxAdapter extends RecyclerView.Adapter<HealthBgChkboxAdapter.ViewHolder> {

    ArrayList<String> options;

    public HealthBgChkboxAdapter(ArrayList<String> options) {
        this.options = options;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_healthbgcheck_box, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("size", "onBindViewHolder: "+options.size());
        holder.textChkbox.setText(options.get(position));
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox)
        CheckBox checkbox;
        @BindView(R.id.text_chkbox)
        TextViewLight textChkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
