package com.ngakauhealth.model;

import java.util.ArrayList;

public class HealthBgModel {
    ArrayList<String> ques;
    ArrayList<String> options;

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    public void setQues(ArrayList<String> ques) {
        this.ques = ques;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public ArrayList<String> getQues() {
        return ques;
    }
}