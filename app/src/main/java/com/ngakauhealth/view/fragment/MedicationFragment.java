package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.adapter.MedicationAdapter;
import com.ngakauhealth.view.base.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MedicationFragment extends BaseFragment {

    @BindView(R.id.text_ques)
    TextViewLight textQues;
    @BindView(R.id.checkbox_yes)
    CheckBox checkboxYes;
    @BindView(R.id.text_chkbox_yes)
    TextViewLight textChkboxYes;
    @BindView(R.id.checkbox_no)
    CheckBox checkboxNo;
    @BindView(R.id.text_chkbox_no)
    TextViewLight textChkboxNo;
    @BindView(R.id.edit_medication)
    EditTextLight editMedication;
    @BindView(R.id.text_count)
    TextViewLight textCount;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    ArrayList<String> reminderList;
    private MedicationAdapter medicationAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_medication, container, false);
        unbinder = ButterKnife.bind(this, view);
        reminderList = new ArrayList<>();
        initRecylerview();

        return view;
    }

    void initRecylerview() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        recyclerView.setNestedScrollingEnabled(false);

        if (reminderList.size() > 0){
            medicationAdapter = new MedicationAdapter(reminderList);
            recyclerView.setAdapter(medicationAdapter);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.img_add, R.id.btn_next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_add:

                reminderList.add(editMedication.getText().toString());
                medicationAdapter.addMedication(reminderList);
                medicationAdapter.notifyDataSetChanged();

                break;
            case R.id.btn_next:

                break;
        }
    }
}
