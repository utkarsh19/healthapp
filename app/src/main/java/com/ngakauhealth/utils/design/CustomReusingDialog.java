package com.ngakauhealth.utils.design;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.ngakauhealth.R;

public class CustomReusingDialog extends DialogFragment {

    public static class Builder {
        private Context context;
        private OnDialogShowListener listener;
        private int dialogLayout;
        private boolean cancelAble;

        public Builder(Context context, OnDialogShowListener mOnDialogShowListener) {
            this.context = context;
            this.listener = mOnDialogShowListener;
        }

        public Builder setDialogLayout(int dialogLayout) {
            this.dialogLayout = dialogLayout;
            return this;
        }

        public Builder setCancelAble(boolean cancelAble) {
            this.cancelAble = cancelAble;
            return this;
        }
    }


    public interface OnDialogShowListener {
        void onGetDialog(final Dialog mDialog, final View mView);
    }


    public static CustomReusingDialog newInstance(Builder mBuilder) {
        CustomReusingDialog frag = new CustomReusingDialog();
        frag.LayoutId = mBuilder.dialogLayout;
        frag.context = mBuilder.context;
        frag.listener = mBuilder.listener;
        frag.cancelAble = mBuilder.cancelAble;
        return frag;
    }

    Dialog dialog;
    private int LayoutId;
    private Context context;
    private OnDialogShowListener listener;
    private boolean cancelAble;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Dialog);
    }


    protected Object mObject;

    public Object getmObject() {
        return mObject;
    }

    public void setmObject(Object mObject) {
        this.mObject = mObject;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        dialog = new Dialog(getActivity(), getTheme()) {
            @Override
            public void dismiss() {
                // Remove soft keyboard
                if (getActivity() != null && getView() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                }
                super.dismiss();
            }
        };
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setWindowAnimations(R.style.dialogFragmentAnimation);
        dialog.setCanceledOnTouchOutside(cancelAble);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(LayoutId, container, false);
        listener.onGetDialog(dialog, view);
        return view;
    }


    @Override
    public void onStop() {
        super.onStop();
    }


}
