package com.ngakauhealth.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ngakauhealth.R;
import com.ngakauhealth.view.base.BaseActivity;
import com.ngakauhealth.view.fragment.LoginFragment;

public class LoginContainerActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logincontainer);



        displayIt(new LoginFragment(), R.id.loginContainer, "", false);

    }


}
