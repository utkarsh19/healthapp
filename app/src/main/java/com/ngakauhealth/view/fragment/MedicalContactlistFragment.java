package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.view.adapter.ExamResultAdapter;
import com.ngakauhealth.view.adapter.MedicalContactListAdapter;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MedicalContactlistFragment extends BaseFragment {


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_add_new_dr)
    ButtonBold btnAddNewDr;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_medical_list, container, false);

        unbinder = ButterKnife.bind(this, view);
        initRecylerview();

        return view;
    }

    void initRecylerview() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        recyclerView.setNestedScrollingEnabled(false);

        MedicalContactListAdapter medicalContactListAdapter = new MedicalContactListAdapter();
        recyclerView.setAdapter(medicalContactListAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_add_new_dr)
    public void onClick() {

        getHomeActivityContainter().displayIt(new AddNewDrFragment(), R.id.home_container, "", true);

    }
}
