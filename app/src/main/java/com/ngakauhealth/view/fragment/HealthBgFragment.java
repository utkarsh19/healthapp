package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.adapter.HealthBgPagerAdapter;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HealthBgFragment extends BaseFragment {


    @BindView(R.id.img_wave)
    ImageView imgWave;
    @BindView(R.id.relative_img)
    RelativeLayout relativeImg;

    @BindView(R.id.text_count)
    TextViewLight textCount;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;
    @BindView(R.id.view_pager)
    ViewPager viewPager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_health_bg, container, false);


        unbinder = ButterKnife.bind(this, view);
        viewPager.setAdapter(new HealthBgPagerAdapter(getChildFragmentManager()));

        return view;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_next)
    public void onClick() {

//        HealthBgAdapter healthBgAdapter = new HealthBgAdapter(getActivity(), setData(1).get(1), 0);
//        recyclerView.setAdapter(healthBgAdapter);
////        Log.d("hiii", "onClick: "+setData().get(0).get(0));

    }


}
