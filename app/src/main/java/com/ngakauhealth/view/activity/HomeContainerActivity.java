package com.ngakauhealth.view.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.ngakauhealth.R;
import com.ngakauhealth.view.base.BaseActivity;
import com.ngakauhealth.view.fragment.BotoomNavFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class HomeContainerActivity extends BaseActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Unbinder unbinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homecontainer);
        unbinder = ButterKnife.bind(this);

        displayIt(new BotoomNavFragment(), R.id.home_container, "", false);


    }


}
