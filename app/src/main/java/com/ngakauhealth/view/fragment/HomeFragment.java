package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.TextViewBold;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends BaseFragment {


    @BindView(R.id.text_health_bg)
    TextViewBold textHealthBg;
    @BindView(R.id.text_exam_result)
    TextViewBold textExamResult;
    @BindView(R.id.text_medication)
    TextViewBold textMedication;
    @BindView(R.id.text_medical_contact)
    TextViewBold textMedicalContact;
    @BindView(R.id.text_my_goal)
    TextViewBold textMyGoal;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);


        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.text_health_bg, R.id.text_exam_result, R.id.text_medication, R.id.text_medical_contact, R.id.text_my_goal})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_health_bg:
                getHomeActivityContainter().displayIt(new HealthBgFragment(), R.id.home_container, "", true);
                break;
            case R.id.text_exam_result:
                getHomeActivityContainter().displayIt(new ExamResutFragment(), R.id.home_container, "", true);
                break;
            case R.id.text_medication:
                getHomeActivityContainter().displayIt(new MedicationFragment(), R.id.home_container, "", true);

                break;
            case R.id.text_medical_contact:
                getHomeActivityContainter().displayIt(new MedicalContactlistFragment(), R.id.home_container, "", true);

                break;
            case R.id.text_my_goal:
                break;
        }
    }
}
