package com.ngakauhealth.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtil {

    public static String PREF_NAME = "LOGOUTPREF";
    public static String PREF1_NAME = "TOKENPREF";


    enum Pref {
        TOKEN
    }


    private static SharedPreferences getSharedPreferences(Context context) {

        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences getTokenSharedPreferences(Context context) {

        return context.getSharedPreferences(PREF1_NAME, Context.MODE_PRIVATE);
    }


    public static String getSaveValue(Context context, String key) {
        return getSharedPreferences(context).getString(key, "");
    }

    public static void setSaveValue(Context context, String key, String value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static int getSaveIntValue(Context context, String key) {
        return getSharedPreferences(context).getInt(key, 0);
    }

    public static void setSaveValue(Context context, String key, int value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(key, value);
        editor.commit();
    }


}
