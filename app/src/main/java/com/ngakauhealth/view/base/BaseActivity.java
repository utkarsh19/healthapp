package com.ngakauhealth.view.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

public class BaseActivity extends AppCompatActivity {


    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    public void displayIt(final Fragment mFragment, final int containerId, final String tag, final boolean isBack) {

        fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();

        if (isBack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction
                .replace(containerId, mFragment, tag)
                .commitAllowingStateLoss();

    }
}
