package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BMIFragment extends BaseFragment {


    @BindView(R.id.edit_date)
    EditTextLight editDate;
    @BindView(R.id.edit_waist_circum)
    EditTextLight editWaistCircum;
    @BindView(R.id.edit_weight)
    EditTextLight editWeight;
    @BindView(R.id.edit_height)
    EditTextLight editHeight;
    @BindView(R.id.btn_next)
    ButtonBold btnNext;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bmi, container, false);


        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_next)
    public void onClick() {
        getHomeActivityContainter().displayIt(new BPFragment(), R.id.home_container, "", true);

    }
}
