package com.ngakauhealth.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ngakauhealth.R;
import com.ngakauhealth.utils.design.ButtonBold;
import com.ngakauhealth.utils.design.EditTextLight;
import com.ngakauhealth.utils.design.TextViewLight;
import com.ngakauhealth.view.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends BaseFragment {


    @BindView(R.id.text_forgot_pass)
    TextViewLight textForgotPass;
    Unbinder unbinder;
    @BindView(R.id.edit_email)
    EditTextLight editEmail;
    @BindView(R.id.edit_pass)
    EditTextLight editPass;
    @BindView(R.id.btn_login)
    ButtonBold btnLogin;
    @BindView(R.id.btn_register)
    ButtonBold btnRegister;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);

        String uData = getString(R.string.forgot_password) + "?";
        SpannableString content = new SpannableString(uData);
        content.setSpan(new UnderlineSpan(), 0, uData.length(), 0);
        textForgotPass.setText(content);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_login, R.id.text_forgot_pass, R.id.btn_register})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:

                break;
            case R.id.text_forgot_pass:
                getLoginActivityContainter().displayIt(new ForgotPassFragment(), R.id.loginContainer, "", true);

                break;
            case R.id.btn_register:
                getLoginActivityContainter().displayIt(new RegisterFragment(), R.id.loginContainer, "", false);
                break;
        }
    }
}
